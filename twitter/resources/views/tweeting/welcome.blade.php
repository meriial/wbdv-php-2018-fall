@extends('layouts/twitter')

@section('content')
  <div class="user-details flex-1 p-10">
    <div class="fz-4 fw-bold mb-0 pb-0 lh-1">
      {{ $user->name }}
    </div>
    <div class="c-3">
      {{ $user->profile->handle }}
    </div>
    <div class="my-3">
      {{ $user->profile->description }}
    </div>
    <div class="c-3">
      <i class="fas fa-link"></i> <a href="">{{ $user->profile->website }}</a>
    </div>
    <div class="c-3">
      Joined {{ $user->created_at->format('M Y') }}
    </div>
  </div>
  <div class="tweets flex-2 bg-white p-10">
    <?php foreach ($tweets as $tweet): ?>
      @include('tweeting/tweet')
    <?php endforeach; ?>
  </div>
  <div class="suggestion flex-1 p-10">
    <div class="fz-3 fw-bold">
      You may also like
    </div>
    <?php foreach ($youMightLike as $tweeter): ?>
      <div class="flex mb-2">
        <div class="mr-2">
            <a href="/<?php echo $tweeter->id ?>">
          <img src="<?php echo $tweeter->profile->image ?>"
            alt=""
            class="circular"
            style="width: 50px"
            >
        </a>
        </div>
        <div class="flex-1">
          <span class="fw-bold"><?php echo $tweeter->name ?></span><br>
          <?php echo $tweeter->profile->handle ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
@endsection

@section('headerBottom')
  <div class="stats bg-white py-10">
    <div class="mr-5 mt-1 position-absolute" style="right: 0; width: 110px">
      <button
        type="button"
        name="button"
        class="btn btn-rounded border border-primary text-primary fw-bold w-100"
      >
        Follow
      </button>
    </div>
    <div class="flex flex-h flex-center flex-1">
      <div class="stat px-10">
        <div class="label fw-bold">
          Tweets
        </div>
        <div class="value">
          <?php echo $user->tweets->count() ?>
        </div>
      </div>
      <div class="stat px-10">
        <div class="label fw-bold">
          Following
        </div>
        <div class="value">
          <?php echo $user->following ?>
        </div>
      </div>
      <div class="stat px-10">
        <div class="label fw-bold">
          Followers
        </div>
        <div class="value">
          <?php echo $user->followers ?>
        </div>
      </div>
      <div class="stat px-10">
        <div class="label fw-bold">
          Likes
        </div>
        <div class="value">
          <?php echo $user->likedTweets->count() ?>
        </div>
      </div>
      <div class="stat px-10">
        <div class="label fw-bold">
          Moments
        </div>
        <div class="value">
          <?php echo $user->moments ?>
        </div>
      </div>
    </div>
  </div>
@endsection
