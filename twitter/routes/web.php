<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// In the real world, the root url is often a splash screen.
// Route::get('/', 'TweetsController@splash');

Route::get('/', 'TweetsController@index');
Route::get('/demo', 'TweetsController@demo');
Route::get('/contact', 'ContactController@index');
Route::get('/tweet/{id}/like', 'TweetsController@toggleLike');

Route::get('/profile', 'ProfileController@index');
Route::post('/profile', 'ProfileController@update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{id}', 'TweetsController@index');
