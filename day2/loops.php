<pre>
<?php

// Given a list
//
$list = [
    'a', 'b', 'c'
];

// Preferred way to iterate: foreach loop
foreach ($list as $value) {
    echo $value."\n";
}

// oldschool for loop
// you can do it, but it's more painful
// I almost never use for-loops
for ($i=0; $i < count($list); $i++) {
    echo $list[$i]."\n";
}

// Given associative array
$bob = [
    'name' => 'Bob',
    'age' => 21
];

echo "\n";

// now we use this syntax to access
// both key (index) AND value
foreach ($bob as $key => $value) {
    echo "$key: $value\n";
}

// let's make another 'person'

$jane = [
    'name' => 'Jane',
    'age' => 24
];

// and create a list of 'people'
$people = [$bob, $jane];

echo "\n";

// we could nest loops like this.
foreach ($people as $value) {
    foreach ($value as $k => $v) {
        echo "$k: $v\n";
    }
}

echo "\n";

// but it often makes more sense to
// iterate like this.
foreach ($people as $person) {
    echo $person['name']
        . ' is ' . $person['age']
        . ' years old.'
        . "\n";
}
