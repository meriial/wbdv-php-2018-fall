<?php

// inconsistent indentation
foreach ($variable as $key => $value) {
    $thing = $foo + $bar;
  $otherThing = 'haha';
            $otherThing = 'haha';
}

// consistent indentation
foreach ($variable as $key => $value) {
    $thing = $foo + $bar;
    $otherThing = 'haha';
    $otherThing = 'haha';
    if (true) {
        watch();
    }
}

class ClassName extends AnotherClass {
    function __construct(argument)
    {
        // code...
    }
}


// bad brace alignment
if (true) {

  }

// good brace alignment
if (true) {

}
