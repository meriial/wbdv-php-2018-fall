<pre>
<?php

// declare an array
$array = [1, 'b', true, 'd', 'B', 'E'];

var_dump($array);

echo "\n";

echo $array[0]."\n";
echo $array[1]."\n";
echo $array[2];

// append elements to array using array_push
array_push($array, 'e', 'f');

echo "\n";
var_dump($array);
echo "\n";

// explicitly set 'q' to end of array
$array[count($array)] = 'q';

// implicitly set 'r' to end of array
$array[] = 'r';

var_dump($array);
echo "\n";

// you can use isset() to check if an index exists
if (isset($array[222])) {
    echo $array[222];
} else {
    echo '222 not there';
}

echo "\n";
if (isset($var)) {
    echo $var;
} else {
    echo '$var not there';
}

// you can overwrite element
$array[2] = 'overwrite!!';
echo "\n";
var_dump($array);
echo "\n";

echo "Sorted array\n";
sort($array);
var_dump($array);
echo "\n";

echo "Custom sorted array\n";
function compare($a, $b) {
    return strtolower($a) <=> strtolower($b);
}

usort($array, 'compare');
var_dump($array);
echo "\n";
