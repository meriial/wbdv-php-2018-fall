@extends('layout')

@section('content')
    <div class="mt-3">
        <h1>Profile Form</h1>
        <div class="mt-3" >
            <form action="/profile" method="post">
                <?php echo csrf_field() ?>
                @include('text_input', [
                    'name' => 'handle',
                    'default' => $profile->handle,
                    'label' => 'Handle'
                ])
                @include('text_input', [
                    'name' => 'description',
                    'default' => $profile->description,
                    'label' => 'Description'
                ])
                <div class="form-group">
                    <input
                        class="form-control"
                        type="text"
                        name="website"
                        {{-- provide the current value of the handle --}}
                        value="<?php echo $profile->website ?>"
                        placeholder="Handle"
                    />
                </div>
                <div class="form-group">
                    <input
                        class="form-control"
                        type="text"
                        name="image"
                        {{-- provide the current value of the handle --}}
                        value="<?php echo $profile->image ?>"
                        placeholder="Handle"
                    />
                </div>

                <input
                    class="btn btn-primary"
                    type="submit"
                    name="submit"
                    value="Submit"
                />
            </form>
        </div>
    </div>
@endsection
