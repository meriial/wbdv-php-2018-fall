<div class="flex mb-3">
  <div class="mr-2">
    <img
      src="<?php echo $tweet->user->profile->image ?>"
      alt=""
      class="circular"
      style="width: 50px"
      >
  </div>
  <div class="">
    <span class="fw-bold"><?php echo $tweet->user->name ?></span>
    <?php echo $tweet->user->handle ?>
    <?php echo $tweet->created_at->format('M j') ?>
    <div class="">
      <?php echo $tweet->content ?>
    </div>
    <div class="">
        <?php if(Auth::check()): ?>
            <a href="/tweet/{{ $tweet->id }}/like">
                <i class="fa fa-heart"></i>
            </a>
        <?php else: ?>
            <i class="fa fa-heart"></i>
        <?php endif; ?>
        {{ $tweet->likes->count() }}
    </div>
  </div>
</div>
