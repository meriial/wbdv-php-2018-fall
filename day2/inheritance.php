<?php

class Cake {
    public $type;

    public function describe()
    {
        return "I'm a $this->type cake.";
    }
}

class ChocolateCake extends Cake {
    public $type = 'chocolate';
}

$cake = new Cake();
echo $cake->describe();
echo '<br>';
$chocCake = new ChocolateCake();
echo $chocCake->describe();


class DatabaseTable {
    public function save(){
        $db = openConnectionToDatabase();
        $sql = 'SAVE into $this->name';
        $db->insert($sql);
    }
    public function fetch(){}
}

class CakeTable extends DatabaseTable {
    public $name = 'cakes';
}

$myTable = new CakeTable();
$myTable->save('data');




//
