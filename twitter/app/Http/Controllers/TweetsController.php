<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Factory;
use App\Models\Tweet;
use App\User;

class TweetsController extends Controller
{
    public function demo()
    {
        $plain = 'password';
        $hashed = md5($plain);
        $hashed2 = md5('something');
        $sha1 = sha1($plain);
        $sha2 = sha1($plain);
        $bestHash = password_hash($plain, PASSWORD_DEFAULT);

        echo '<pre>';
        var_dump( $plain, $hashed, $hashed2, $sha1, $sha2, $bestHash );

        $isGood = password_verify($plain, $bestHash);

        var_dump( $isGood );

        return '';
    }

    public function splash()
    {
        return 'Welcome to Twitter!';
    }

    public function index($id = 1)
    {
        $primaryUser = $this->getPrimaryUser($id);
        $youMightLikeUsers = $this->getYouMightLikeUsers($primaryUser);
        $tweets = $this->getTweets($primaryUser);

        $viewData = [
            'user' => $primaryUser,
            'youMightLike' => $youMightLikeUsers,
            'tweets' => $tweets,
        ];

        return view('tweeting/welcome', $viewData);
    }

    public function getPrimaryUser($id)
    {
        $primaryUser = User::findOrFail($id);

        $primaryUser->tweetCount = '2,154';
        $primaryUser->following = 53;
        $primaryUser->followers = '172K';
        $primaryUser->likes = 402;
        $primaryUser->moments = 2;

        return $primaryUser;
    }

    public function getYouMightLikeUsers($primaryUser)
    {
        return User::where('id', '!=', $primaryUser->id)->get();
    }

    public function getTweets($primaryUser)
    {
        $tweets = Tweet::where('user_id', $primaryUser->id)->get();

        return $tweets;
    }

    public function toggleLike($tweetId)
    {
        $user = request()->user();

        if (!$user) {
            return redirect('/');
        }

        if ($user->likedTweets->contains($tweetId)) {
            $user->likedTweets()->detach($tweetId);
        } else {
            $user->likedTweets()->attach($tweetId);
        }

        return redirect()->back();
    }
}
