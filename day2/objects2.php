<?php

class Cake {
    private $type;
    private $secretIngredient;

    public function __construct($type, $secretIngredient)
    {
        $this->type = $type;
        $this->secretIngredient = $secretIngredient;
    }

    public function describeTaste() {
        return 'Yummy '.$this->type.'!';
    }

    public function recipe()
    {
        return $this->secretIngredient;
    }
}

$carrotCake = new Cake('carrot', 'sludge');
echo $carrotCake->describeTaste().'<br/>';
echo $carrotCake->recipe().'<br/>';

$chocCake = new Cake('chocolate', 'ginger');
echo $chocCake->describeTaste().'<br/>';
echo $chocCake->recipe().'<br/>';








//
