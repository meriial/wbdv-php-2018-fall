<pre>
<?php

$a = 2;
$b = 2.4;
$c = true;
$d = "something";
$e = [1, 2, "foo"];
$f = new stdClass();
$g = function() {
    return 'Hi';
};

var_dump($a, $b, $c, $d, $e, $f, $g);

define('CONSTANT', 13);

var_dump(CONSTANT);

define('CONSTANT', 11);
