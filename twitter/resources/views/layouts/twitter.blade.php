<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
  </head>
  <body>
    <header class="flex flex-v bg-white">
      <div class="container bg-white p-10 flex justify-content-end">
        <?php if(Auth::check()): ?>
            <div class="mr-3">
                Hello, <a href="/<?php echo request()->user()->id ?>"><?php echo request()->user()->name ?></a>!
            </div>
            <a href="/profile" class="mr-3">Profile</a>
            @include('logout')
        <?php else: ?>
            <a href="/login">Login</a>
        <?php endif; ?>
      </div>
      <div class="hero-row bg-2">
        <img src="https://pbs.twimg.com/profile_banners/158331222/1528372321/1500x500" width="100%" height="300px" style="object-fit: cover;">
      </div>
      @yield('headerBottom')
    </header>
    <main class="flex full-page container">
      @yield('content')
    </main>
  </body>
</html>
