<pre>
<?php

$bob = [
    'name' => 'Bob',
    'age' => 21
];

$bobObject = (object) $bob;

var_dump($bobObject);

echo "\n";


class Cake {
    public $type = 'chocolate';
    private $secretIngredient = 'ginger';

    public function describeTaste() {
        return 'Yummy!';
    }
}

$cake = new Cake();
echo $cake->describeTaste();
// in javascript it would look like cake.describeTaste()
echo "\n";
$cake->type = 'vanilla';
echo $cake->type;
echo "\n";
// echo $cake->secretIngredient;
echo "\n";
var_dump($cake);







//
